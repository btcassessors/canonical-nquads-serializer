#!/bin/bash

docker build -t registry.gitlab.com/btcassessors/canonical-nquads-serializer .
docker push registry.gitlab.com/btcassessors/canonical-nquads-serializer
