import json

from cert_schema import jsonld_helpers
from cert_schema import normalize_jsonld
from flask import Flask, request, Response

# Constants
app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def hello():
    return Response(normalize_jsonld(request.get_json()), mimetype="application/n-quads")
