FROM python:3.7-alpine

COPY requirements.txt .
COPY nquads.py .

RUN pip install -r requirements.txt

EXPOSE 5000

ENTRYPOINT FLASK_APP=nquads.py flask run -h 0.0.0.0
